# BattlebornCharacterData
Character data for Battleborn in JSON format

## Todo:
* Add ability cooldowns
* Add health and shield values (do these scale with level? for everyone?)
* Add ability level scaling data (assuming they scale?)
