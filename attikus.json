{
    "abilities": {
        "ability1": {
            "description": "Leap to a target location, dealing 67 damage and knocking back enemies where Attikus lands. Fully Charged: Deals 100 damage to shields.",
            "name": "Pounce"
        },
        "ability2": {
            "description": "Electrifies Attikus for 8 seconds. Damaged enemies are shocked, taking 8 damage over 4 seconds. Fully Charged: Shock also damages nearby enemies.",
            "name": "Hedronic Arc"
        },
        "passive": {
            "description": "Attikus gains 1 charge per kill (5 for major enemies) up to a maximum of 5. While Fully Charged the next skill use consumes all charges for an added effect.",
            "name": "Hedronic Collector"
        },
        "primary": {
            "description": "Attikus' primary attack is a relentless bare-handed assault using both fists. Fully Charged: Attacks gain 25% shield penetration.",
            "name": "Barefisted Brawling"
        },
        "talent": {
            "description": "Attikus' second attack is a devastating right hook that can be charged-up for additional damage.",
            "name": "Charged Hook"
        },
        "ultimate": {
            "description": "Attikus hurls 10 shockwaves of energy over 5 seconds that each deal 84 damage. Fully Charged: Enemies are also knocked into the air.",
            "name": "Hedronic Eruption"
        }
    },
    "characteristics": [
        "disruptor",
        "brawler",
        "easy"
    ],
    "faction": "Jennerit",
    "helixTalents": {
        "1": [
            {
                "affects": "ability2",
                "description": "A portion of damage dealt by Hedronic Arc is returned to Attikus as health.",
                "name": "Hedronic Siphoning",
                "summary": "+15% Life Steal"
            },
            {
                "affects": "ability1",
                "description": "Pounce deals additional damage in an area of effect around Attikus at the beginning of his leap.",
                "name": "Energetic Burst",
                "summary": "+50% Damage"
            }
        ],
        "2": [
            {
                "affects": "ability1",
                "description": "Enemies damaged by Pounce are slowed.",
                "name": "Staggering Pounce",
                "summary": "+3 Seconds Slow Duration"
            },
            {
                "affects": "ability1",
                "description": "Pounce gives Attikus life steal on all damage dealt for a short time.",
                "name": "Invigorating Pounce",
                "summary": "+30% Life Steal for 8 Seconds"
            },
            {
                "affects": "ability1",
                "description": "Activating Pounce causes Attikus shields to immediately begin rapidly recharging until impact.",
                "name": "Energetic Pounce",
                "summary": "+105 Shield Recharge Per Second"
            }
        ],
        "3": [
            {
                "affects": "primary",
                "description": "A portion of damage dealt by Attikus' melee attacks is returned to him as health.",
                "name": "Brawler's Boon",
                "summary": "+15% Life Steal"
            },
            {
                "affects": "primary",
                "description": "Repeated melee damage to the same target within 2 seconds stacks bonus damage up to 5 times.",
                "name": "Tenacity",
                "summary": "Up to +60% Damage"
            }
        ],
        "4": [
            {
                "affects": "ability2",
                "description": "Enemies damaged by Hedronic Arc suffer reduced damage output for a short time.",
                "name": "Dampening Field",
                "summary": "-30% Enemy Damage Output for 1 Second"
            },
            {
                "affects": "ability2",
                "description": "Attikus moves faster while Hedronic Arc is shocking enemies.",
                "name": "Quickening Arc",
                "summary": "+30% Movement Speed"
            }
        ],
        "5": [
            {
                "affects": "passive",
                "description": "Attikus gains a small boost to health regeneration for each charge accumulated by Hedronic Collector.",
                "name": "Hedronic Regeneration",
                "summary": "Up to +35 Health Regeneration Per Second"
            },
            {
                "affects": "passive",
                "description": "Executing a Fully Charged skill consumes 1 charge instead of 5, but Attikus only gains 1 charge per major enemy kill instead of 5.",
                "name": "Charge Efficiency"
            },
            {
                "affects": "passive",
                "description": "Attikus' skill cooldowns are reduced slightly for each charge accumulated by Hedronic Collector.",
                "name": "Hedronic Haste",
                "summary": "Up to -15% Cooldown Time"
            }
        ],
        "6": [
            {
                "affects": "ability2",
                "description": "Increases Hedronic Arc damage against enemy shields.",
                "name": "Disruptor Field",
                "summary": "+50% Bonus Damage To Shields"
            },
            {
                "affects": "ability1",
                "description": "If Pounce kills a major enemy the skill's cooldown is immediately reset.",
                "name": "Chain Breaker"
            }
        ],
        "7": [
            {
                "affects": "talent",
                "description": "Increases Attikus' maximum health.",
                "name": "Unstoppable",
                "summary": "+360 Maximum Health"
            },
            {
                "affects": "primary",
                "description": "Increases damage dealt by critical hits.",
                "name": "Skull Crusher",
                "summary": "+20% Critical Damage"
            },
            {
                "affects": "primary",
                "description": "Increases Attikus' attack speed.",
                "name": "Swift Strikes",
                "summary": "+20% Attack Speed"
            }
        ],
        "8": [
            {
                "affects": "ability1",
                "description": "Increases Pounce's area of effect.",
                "name": "Big Splash",
                "summary": "+50% Area of Effect Radius"
            },
            {
                "affects": "ability1",
                "description": "Increases Pounce's damage.",
                "name": "Power Pounce",
                "summary": "+15% Damage"
            }
        ],
        "9": [
            {
                "affects": "ability2",
                "description": "Hedronic Arc chains to nearby enemies, dealing reduced damage to the additional targets.",
                "name": "Hedronic Chain",
                "summary": "50% Damage To Nearby Targets"
            },
            {
                "affects": "ability2",
                "description": "Increases the damage per second dealt by Hedronic Arc but reduces its duration.",
                "name": "Greased Lightning",
                "summary": "+100% Damage per Second, -2 Seconds Duration"
            },
            {
                "affects": "ability2",
                "description": "Increases the amount of time that enemies take damage when shocked by Hedronic Arc.",
                "name": "Aftershock",
                "summary": "+2 Seconds Duration"
            }
        ],
        "10": [
            {
                "affects": "ultimate",
                "description": "Hedronic Eruption scorches the earth, dealing damage over time to enemies in its area of effect.",
                "name": "Wake of Devastation",
                "summary": "+240 Damage over 2 Seconds"
            },
            {
                "affects": "ultimate",
                "description": "Each Hedronic Eruption shockwave deals more damage the lower Attikus' current health is.",
                "name": "Payback Time",
                "summary": "Up to +60 Damage"
            },
            {
                "affects": "ultimate",
                "description": "Each enemy killed by Hedronic Eruption prolongs its duration.",
                "name": "Unstoppable Rampage",
                "summary": "+2 Seconds per Kill, Up to +10 Seconds Duration"
            }
        ],
        "titles": [
          "Pit Fighter",
          "Skull Crusher"
        ]
    },
    "name": "Attikus",
    "role": "Defender"
}
